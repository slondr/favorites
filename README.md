# Favorites

This is the front-end for the Favorites page of EricLondres.tech.

It is written in Svelte and deployed via Surge.

To start a local dev version:

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see the app running. Live updates to components are enabled.

Favorites is free software, licensed under the terms of the GNU Affero GPL.
